# git-building-pandoc

Some pandoc filters (written in Python) to work alongside [git-building](https://gitlab.com/bath_open_instrumentation_group/git-building/), particularly useful for PDF generation.