"""
This file is intended to be a pandoc filter that will find and copy in relative links to other markdown files.

The intended use is to convert a single-file git-building instruction into a single PDF.  When I say "single-file",
I mean only one set of instructions - typically each part will be a markdown file.  This is the case when using
either markdown files or yaml files to store parts, as both are converted to markdown by git-building.

There are plenty TODOs:

TODO: new page for each section (in included files) https://jdhao.github.io/2019/05/30/markdown2pdf_pandoc/#how-to-start-a-new-page-for-each-section
TODO: process links in included files (currently they are just left broken)
TODO: compare relative links in a smarter way (e.g. so that ./foo.md and foo.md are equivalent)
TODO: make some effort to format images nicely?
"""

import panflute as pf
import pypandoc
import argparse
import re
import io
import os

def prepare(doc):
    doc.pages_to_include = []
    doc.link_conversions = {}
    doc.included_pages = {}

def log_relative_links(elem, doc):
    """Build up a list of all the relative links in the document"""
    if isinstance(elem, pf.Link):
        if "://" not in elem.url and elem.url.endswith("md"):
            if elem.url not in doc.pages_to_include:
                doc.pages_to_include.append(elem.url)
    return elem

def convert_relative_links(elem, doc):
    """Convert relative links to use the stored IDs"""
    if isinstance(elem, pf.Link):
        if elem.url in doc.link_conversions:
            elem.url = "#" + doc.link_conversions[elem.url]
    return elem

def increment_header_level(elem, doc):
    """Increment the header level by one, for embedding documents"""
    if isinstance(elem, pf.Header):
        elem.level += 1
    return elem

def find_or_create_first_header(doc, text):
    """Find the first L1 header in a document, or prepend a new one.

    If no header is found, we'll prepend a new one, with the supplied text.

    Returns: doc, header
    """
    first_header = None
    for elem in doc.content:
        if isinstance(elem, pf.Header):
            if elem.level==1:
                first_header = elem
    if first_header is None:
        doc.content.insert(0,pf.Header(text))
        first_header = doc.content[0]
    return doc, first_header

class Bunch:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

def markdown_file_to_doc(filename):
    """Load a file, parse it with pandoc, and create a panflute document"""
    data = pypandoc.convert_file(filename, "json")
    doc = pf.load(io.StringIO(data))
    return doc

def parse_included_pages(doc):
    """Load linked pages from disk and process them so they can be linked to.

    NB this populates doc.link_conversions and doc.included_pages but does not
    modify the document, i.e. the pages are not yet included.
    """
    for page in doc.pages_to_include:
        print(f"Converting links to {page}")
        # We need to generate it an internal ID so we can link to it
        if page not in doc.link_conversions:
            doc.link_conversions[page] = f"automatically-included-page-{len(doc.link_conversions)}"
        
        # Now we parse the file
        idoc = markdown_file_to_doc(page)
        
        # Set the identifier of the first header in the document, so we can link to it.
        # If there is no level 1 header, create one.
        idoc, first_header = find_or_create_first_header(idoc, page)
        first_header.identifier=doc.link_conversions[page]

        # Increment the header level of all headers in the included document to avoid cluttering the ToC
        idoc = pf.run_filter(increment_header_level, doc=idoc)

        # Save the included document in the included_pages dictionary for now
        doc.included_pages[page] = idoc
    return doc

def embed_included_pages(doc):
    """Embed the pages that are stored in doc.included_pages in the actual document."""
    doc.content.append(pf.Header(pf.Str("Included Files")))
    doc.content.append(pf.Para(pf.Str("""After this section are other files, usually descriptions of parts and tools.""")))
    for url, idoc in doc.included_pages.items():
        doc.content.append(pf.HorizontalRule)
        doc.content.extend(idoc.content)
    return doc

def main():
    #TODO: argparse... parser = argparse.ArgumentParser("")
    parser = argparse.ArgumentParser(description="Embed linked files into a markdown document, and generate a PDF")
    parser.add_argument("input_file")
    args = parser.parse_args()
    include_first_level_links(args.input_file)

def include_first_level_links(input_file):
    # First, load the markdown file, parse it with pandoc, and turn it into a panflute document
    doc = markdown_file_to_doc(input_file)

    # Next, pull out all the relative links to markdown files into doc.pages_to_include
    doc = pf.run_filter(log_relative_links, prepare=prepare, doc=doc)

    # Now, generate internal IDs for each included page, and load in the markdown files for each page
    doc = parse_included_pages(doc)
    
    # Convert the links in the main file
    # TODO: make this work for the included pages as well (currently stored in included_pages)
    doc = pf.run_filter(convert_relative_links, doc=doc)

    # Now add the included pages to the output
    doc = embed_included_pages(doc)
    
    # Finally, render it as a PDF
    with io.StringIO() as f:
        pf.dump(doc, f)
        pypandoc.convert_text(f.getvalue(), format="json", to="md", outputfile=os.path.splitext(input_file)[0] + "_combined.md")
        pypandoc.convert_text(f.getvalue(), format="json", to="pdf", outputfile=os.path.splitext(input_file)[0] + "_combined.pdf")


if __name__ == '__main__':
    main()


